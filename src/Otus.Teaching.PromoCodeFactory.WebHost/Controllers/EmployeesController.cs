﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создание
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> Create(Guid id, string fullName, string email)
        {
           List<Employee> employees = (await _employeeRepository.GetAllAsync()).ToList();

           var FullName = fullName.Split(" ");

           var newEmployee=new Employee();

           newEmployee.Id = id;
           newEmployee.FirstName = FullName[0];
           newEmployee.LastName = FullName[1];
           newEmployee.Email = email;

           newEmployee.Roles = new List<Role>()
           {
               Otus.Teaching.PromoCodeFactory.DataAccess.Data.FakeDataFactory.Roles.FirstOrDefault(x => x.Name == "PartnerManager")
           };

           newEmployee.AppliedPromocodesCount = 0;

           employees.Add(newEmployee);

           return new ActionResult<EmployeeResponse>(Ok());


        }

        /// <summary>
        /// Удаление по <see cref="id"/>
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> Delete(Guid id)
        {
            List<Employee> employees = (await _employeeRepository.GetAllAsync()).ToList();
            Employee employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            employees.RemoveAt(employees.IndexOf(employee));


            return new ActionResult<EmployeeResponse>(Ok());

        }



        /// <summary>
        /// Редактирование по <see cref="id"/>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fullName"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> Update(Guid id, string fullName, string email)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var FullName = fullName.Split(" ");

            employee.FirstName = FullName[0];
            employee.LastName= FullName[1];
            employee.Email= email;



            return new ActionResult<EmployeeResponse>(Ok());
        }

        #region Данные для теста


        //      {
        //  "id": "451533d5-d8d5-4a11-9c7b-eb9f14e1a35f",
        //  "fullName": "Тигра Пятый",
        //  "email": "Crocs@somemail.ru"
        //}


        //{
        //  "id": "451533d5-d8d5-4a12-9c7b-eb9f14e1a35f",
        //  "fullName": "Кошка Первая",
        //  "email": "Cat@somemail.ru"
        //}

          #endregion

    }
}